/**
 * @file                 init.h
 * @brief                初始化相关文件
 * @details             使用EasyIO Framework 需要引入此头文件，此文件包含了所有初始化相关的功能函数
 * @author               stopfan
 * @date         2013-11-11
 * @version      A001
 * @par Copyright (c): 
 *               新动力科技
 * @par History:         
 *       version: stopfan, 2013-12-12, desc\n
 */

#ifndef __init_h__
#define __init_h__

/** 初始化mode type
 *
 * @param[in] option : 模块类型参数
 *
 * @return
 **/
EIResult_t InitModem(struct ModemOption *option);


/** 初始化GPRS网络
 *
 * @param[in] option : GPRS网络参数
 *
 * @return
 **/
EIResult_t InitGprsNetwork(struct GprsNetworkOption *option);

/** 初始化Dtu
 *
 * @param[in] option : Dtu参数
 *
 * @return
 **/
EIResult_t InitTcpcli(struct TcpcliOption *option);

/** 初始化EasyIO云,初始化此功能后，可使用微信，Web，手机等 设备与设备进行通讯
 *
 *  @param[in] pwd : 设备在云端访问的密码
 *
 * @return
 **/
EIResult_t InitEasyIOCloud(struct CloudOption *option);


#endif
