#ifndef __cloud_api_h__
#define __cloud_api_h__

typedef struct cloud_api {
	EIResult_t (*ServiceRQ)(const char *req , int timeout , void (*response)(void *arg));

}CLOUD_API;

#endif
