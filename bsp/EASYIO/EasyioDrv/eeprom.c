#include "stm32f10x.h"
#include "stm32f10x_i2c.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "eeprom.h"
#include <rtthread.h>

#define PUT_2(...)
#define PUT(...)

#define AT24C01A

#define EEPROM_ADDR		0xA0
#define I2C_PAGESIZE            4		//24C01/01A页缓冲是4个

static struct rt_semaphore eeprom_sem; // 信号量

/*-----------------------------------------------------------------------
  创思通信 http://tpytongxin.taobao.com/

  编写日期          ：2012-8-10
  最后修改日期      ：2012-8-13
  -----------------------------------------------------------------------*/
void I2C_Configuration(void)
{
	I2C_InitTypeDef  I2C_InitStructure;
	GPIO_InitTypeDef  GPIO_InitStructure;

	rt_sem_init(&eeprom_sem, "eeprom_sem", 1, RT_IPC_FLAG_FIFO);


	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);


	/* PB6,7 SCL and SDA */
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	I2C_DeInit(I2C1);
	I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_OwnAddress1 = 0x30;
	I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed = 100000;//100K速度

	I2C_Cmd(I2C1, ENABLE);
	I2C_Init(I2C1, &I2C_InitStructure);

	/*允许1字节1应答模式*/
	I2C_AcknowledgeConfig(I2C1, ENABLE);

	PUT_2("I2C_Configuration \r\n");
}

/***************************************************
 **函数名:I2C_ReadS
 **功能:读取24C02多个字节
 **注意事项:24C02是256字节,8位地址,A0-A2固定为0,从器件地址为EEPROM_ADDR
 ***************************************************/
void I2C_ReadS_24C(u8 addr ,u8* pBuffer,u16 no)
{
	if(no==0)
		return;

	rt_sem_take(&eeprom_sem, RT_WAITING_FOREVER);

	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));

	/*允许1字节1应答模式*/
	I2C_AcknowledgeConfig(I2C1, ENABLE);


	/* 发送起始位 */
	I2C_GenerateSTART(I2C1, ENABLE);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));/*EV5,主模式*/

#ifdef AT24C01A
	/*发送器件地址(写)*/
	I2C_Send7bitAddress(I2C1,  EEPROM_ADDR, I2C_Direction_Transmitter);
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

	/*发送地址*/
	I2C_SendData(I2C1, addr);
	while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));/*数据已发送*/

	/*起始位*/
	I2C_GenerateSTART(I2C1, ENABLE);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));

	/*器件读*/
	I2C_Send7bitAddress(I2C1, EEPROM_ADDR, I2C_Direction_Receiver);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));


#else
	/*发送器件地址(读)24C01*/
	I2C_Send7bitAddress(I2C1, addr<<1, I2C_Direction_Receiver);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
#endif

	while (no)
	{
		if(no==1)
		{
			I2C_AcknowledgeConfig(I2C1, DISABLE);	//最后一位后要关闭应答的
			I2C_GenerateSTOP(I2C1, ENABLE);			//发送停止位
		}

		while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED)); /* EV7 */
		*pBuffer = I2C_ReceiveData(I2C1);
		pBuffer++;
		/* Decrement the read bytes counter */
		no--;
	}
	//再次允许应答模式
	I2C_AcknowledgeConfig(I2C1, ENABLE);
	rt_sem_release(&eeprom_sem);
}

/****************************************************
 **函数名:I2C_Standby_24C
 **功能:24C是否准备好再写入的判断
 **注意事项:本函数可以理解为:判忙
 ****************************************************/
void I2C_Standby_24C(void)
{
	vu16 SR1_Tmp;
	do
	{
		/*起始位*/
		I2C_GenerateSTART(I2C1, ENABLE);
		/*读SR1*/
		SR1_Tmp = I2C_ReadRegister(I2C1, I2C_Register_SR1);
		/*器件地址(写)*/
#ifdef AT24C01A
		I2C_Send7bitAddress(I2C1, EEPROM_ADDR, I2C_Direction_Transmitter);
#else
		I2C_Send7bitAddress(I2C1, 0, I2C_Direction_Transmitter);
#endif
	}while(!(I2C_ReadRegister(I2C1, I2C_Register_SR1) & 0x0002));

	/**/
	I2C_ClearFlag(I2C1, I2C_FLAG_AF);
	/*停止位*/
	I2C_GenerateSTOP(I2C1, ENABLE);
}

/*************************************************
 **函数名:I2C_ByteWrite_24C
 **功能:写一个字节
 **注意事项:字写入同样需要调用忙判断
 *************************************************/
void I2C_ByteWrite_24C(u8 addr,u8 dat)
{
	/* 起始位 */
	rt_sem_take(&eeprom_sem, RT_WAITING_FOREVER);
	I2C_GenerateSTART(I2C1, ENABLE);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));

#ifdef AT24C01A
	/* 发送器件地址(写)*/
	I2C_Send7bitAddress(I2C1, EEPROM_ADDR, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

	/*发送地址*/
	I2C_SendData(I2C1, addr);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

#else
	I2C_Send7bitAddress(I2C1, addr<<1, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
#endif

	/* 写一个字节*/
	I2C_SendData(I2C1, dat);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

	/* 停止位*/
	I2C_GenerateSTOP(I2C1, ENABLE);

	I2C_Standby_24C();

	rt_sem_release(&eeprom_sem);

}

/*************************************************
 **函数名:I2C_PageWrite_24C
 **功能:写入一页(以内)
 **注意事项:此函数供群写入调用
 *************************************************/
void I2C_PageWrite_24C(u8 addr,u8* pBuffer, u8 no)
{
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));

	/*起始位*/
	I2C_GenerateSTART(I2C1, ENABLE);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));

#ifdef AT24C01A
	/*器件地址(写)*/
	I2C_Send7bitAddress(I2C1, EEPROM_ADDR, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));

	/*写地址值*/
	I2C_SendData(I2C1, addr);
	while(! I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

#else
	I2C_Send7bitAddress(I2C1, addr<<1, I2C_Direction_Transmitter);
	while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
#endif
	while(no--)
	{
		I2C_SendData(I2C1, *pBuffer);
		pBuffer++;
		while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
	}

	/*停止位*/
	I2C_GenerateSTOP(I2C1, ENABLE);
}



/*************************************************
 **函数名:I2C_WriteS_24C
 **功能:页写入24C
 **注意事项:24C02最多允许一次写入8个字节
 *************************************************/
void I2C_WriteS_24C(u8 addr,u8* pBuffer,  u16 no)
{
	u8 temp;

	//1.先把页不对齐的部分写入
	temp=addr % I2C_PAGESIZE;
	if(temp)
	{
		temp=I2C_PAGESIZE-temp;
		I2C_PageWrite_24C(addr,pBuffer,  temp);
		no-=temp;
		addr+=temp;
		pBuffer+=temp;
		I2C_Standby_24C();
	}
	//2.从页对齐开始写
	while(no)
	{
		if(no>=I2C_PAGESIZE)
		{
			I2C_PageWrite_24C(addr,pBuffer,  I2C_PAGESIZE);
			no-=I2C_PAGESIZE;
			addr+=I2C_PAGESIZE;
			pBuffer+=I2C_PAGESIZE;
			I2C_Standby_24C();
		}
		else
		{
			I2C_PageWrite_24C(addr,pBuffer,  no);
			no=0;
			I2C_Standby_24C();
		}
	}
}

/*-----------------------------------------------------------------------
  创思通信 http://tpytongxin.taobao.com/

  编写日期          ：2012-8-10
  最后修改日期      ：2012-8-13
  -----------------------------------------------------------------------*/
#if 0
void I2C_Test(void)
{
	u8 i;
	u8 I2c_Buf[256];
	int strlength = 0;

	//填充缓冲
	for(i=0;i<255;i++)
		I2c_Buf[i]=0;

	strlength = strlen(TESTIC);

	//写
	PUT("Write\n\r");
	I2C_WriteS_24C(1,TESTIC,strlength);

	//清缓冲
	for(i=0;i<255;i++)
		I2c_Buf[i]=0;

	//读
	PUT("Read\n\r");
	I2C_ReadS_24C(1,I2c_Buf,strlength);


	for(i=0;i<254;i++)
	{
		debug_put_word(I2c_Buf[i]);
	}
}

/*-----------------------------------------------------------------------
  创思通信 http://tpytongxin.taobao.com/

  编写日期          ：2012-8-10
  最后修改日期      ：2012-8-13
  -----------------------------------------------------------------------*/
void write_data_eeprom(char* data)
{
	char* p = NULL;
	int strlength;

	I2C_WriteS_24C(1,(u8*)p,USE_EEPROM_DATA_LENGTH);    //clean

	strlength = strlen((const char*)data);
	I2C_WriteS_24C(1,(u8*)data,strlength);
}

/*-----------------------------------------------------------------------
  创思通信 http://tpytongxin.taobao.com/

  编写日期          ：2012-8-10
  最后修改日期      ：2012-8-13
  -----------------------------------------------------------------------*/
char* read_data_eeprom(void)
{
	memset(I2c_Buf,0,USE_EEPROM_DATA_LENGTH);
	I2C_ReadS_24C(1,I2c_Buf,USE_EEPROM_DATA_LENGTH);

	if(!(char *)I2c_Buf)
	{
		PUT("no data write default data\r\n");
		return NULL;
	}
	return (char *)I2c_Buf;
}

/*-----------------------------------------------------------------------
  创思通信 http://tpytongxin.taobao.com/

  编写日期          ：2012-8-10
  最后修改日期      ：2012-8-13
  -----------------------------------------------------------------------*/
void write_config_data_to_eeprom_api(char *data)
{
	PUT(data);
	write_data_eeprom(data);
}

/*-----------------------------------------------------------------------
  创思通信 http://tpytongxin.taobao.com/

  编写日期          ：2012-8-10
  最后修改日期      ：2012-8-13
  -----------------------------------------------------------------------*/
void parser_config_data(char *data)
{
	int str_st, str_end, len;
	char line[USE_EEPROM_DATA_LENGTH];          //line str source
	char des_line[EEPROM_FILED_LENGTH];         //seg str
	char *pt=NULL;

	memset(line,0,USE_EEPROM_DATA_LENGTH);
	memset(des_line,0,EEPROM_FILED_LENGTH);



	memset(g_config_data.serialnum,0,EEPROM_FILED_LENGTH);
	memset(g_config_data.protocoltype,0,EEPROM_FILED_LENGTH);
	memset(g_config_data.ipaddr,0,EEPROM_FILED_LENGTH);
	memset(g_config_data.portnum,0,EEPROM_FILED_LENGTH);
	memset(g_config_data.telnum,0,EEPROM_FILED_LENGTH);
	g_config_data.bufsize = 0;

	if(strlen(data) < 20)
	{
		set_default_config_data();
		return ;
	}

	pt = strstr(data,"$");

	if(!pt)
	{
		set_default_config_data();
		return ;
	}


	sprintf(line,"%s",data);

	PUT_2("line");
	PUT_2(line);
	PUT_2("\r\n");

	str_st = GetComma(1,line);
	str_end = GetComma(2,line);
	len = str_end - str_st;
	memset(des_line,0,EEPROM_FILED_LENGTH);

	strncpy(des_line, &line[str_st], len-1);  // get serial


	sprintf(g_config_data.serialnum,"%s",des_line);

	PUT("**************************\r\n");
	PUT("CE1:");
	PUT(g_config_data.serialnum);
	PUT("\r\n");

	str_st = GetComma(2,line);
	str_end = GetComma(3,line);

	len = str_end - str_st;
	memset(des_line,0,EEPROM_FILED_LENGTH);

	strncpy(des_line, &line[str_st], len-1);  // protocoltype

	sprintf(g_config_data.protocoltype,"%s",des_line);

	PUT("CE2:");
	PUT(g_config_data.protocoltype);
	PUT("\r\n");

	str_st = GetComma(3,line);
	str_end = GetComma(4,line);
	len = str_end - str_st;
	memset(des_line,0,EEPROM_FILED_LENGTH);
	strncpy(des_line, &line[str_st], len-1);  // ipaddr
	sprintf(g_config_data.ipaddr,"%s",des_line);

	PUT("CE3:");
	PUT(g_config_data.ipaddr);
	PUT("\r\n");

	str_st = GetComma(4,line);
	str_end = GetComma(5,line);
	len = str_end - str_st;
	memset(des_line,0,EEPROM_FILED_LENGTH);
	strncpy(des_line, &line[str_st], len-1);  // portnum
	sprintf(g_config_data.portnum,"%s",des_line);

	PUT("CE4:");
	PUT(g_config_data.portnum);
	PUT("\r\n");

	str_st = GetComma(5,line);
	str_end = GetComma(6,line);
	len = str_end - str_st;
	memset(des_line,0,EEPROM_FILED_LENGTH);
	strncpy(des_line, &line[str_st], len-1);  // telnum
	sprintf(g_config_data.telnum,"%s",des_line);

	PUT("CE5:");
	PUT(g_config_data.telnum);
	PUT("\r\n");

	str_st = GetComma(6,line);
	str_end = GetComma(7,line);
	len = str_end - str_st;
	memset(des_line,0,EEPROM_FILED_LENGTH);
	strncpy(des_line, &line[str_st], len-1);  //bufsize

	PUT("CE6:");
	PUT(des_line);
	PUT("\r\n");
	PUT("**************************\r\n");
	if(strlen(des_line) > 0)
		g_config_data.bufsize = atoi(des_line);
}


/*-----------------------------------------------------------------------
  创思通信 http://tpytongxin.taobao.com/

  编写日期          ：2012-8-10
  最后修改日期      ：2012-8-13
  -----------------------------------------------------------------------*/
void set_default_config_data(void)
{
#if 0
	sprintf(g_config_data.serialnum,"%s",SERNUM);
	sprintf(g_config_data.protocoltype,"%s",PROTOTOCOL);
	sprintf(g_config_data.ipaddr,"%s",IPADDR);
	sprintf(g_config_data.portnum,"%s",PORTNUM);
	sprintf(g_config_data.telnum,"%s",TELNUM);
	g_config_data.bufsize = CACHESIZE;
#endif
}



/*-----------------------------------------------------------------------
  创思通信 http://tpytongxin.taobao.com/

  编写日期          ：2012-8-10
  最后修改日期      ：2012-8-13
  -----------------------------------------------------------------------*/
unsigned char GetComma(unsigned char num,char *str)
{

#if 0
	unsigned char i,j = 0;
	int len=strlen(str);
	for(i = 0;i < len;i ++)
	{
		if(str[i] == ',')
			j++;
		if(j == num)
			return i + 1;
		if(j > MAX_COMMA)         //如果超时
			return 0;
	}
#endif
	return 0;
}
#endif
