#ifndef __STM32_EEPROM_H_
#define __STM32_EEPROM_H_

#define u8 unsigned char
#define u16 unsigned short

void I2C_Configuration(void);
void I2C_ReadS_24C(u8 addr ,u8* pBuffer,u16 no);
void I2C_Standby_24C(void);
void I2C_ByteWrite_24C(u8 addr,u8 dat);
void I2C_PageWrite_24C(u8 addr,u8* pBuffer, u8 no);
void I2C_WriteS_24C(u8 addr,u8* pBuffer,  u16 no);
void I2C_Test(void);

#define EEPROM_FILED_LENGTH 64
typedef struct _EEPROM_CONFIG_DATA
{
    char serialnum[EEPROM_FILED_LENGTH+1];          //系列号
    char protocoltype[EEPROM_FILED_LENGTH+1];       //协议类型
    char ipaddr[EEPROM_FILED_LENGTH+1];             //IP地址
    char portnum[EEPROM_FILED_LENGTH+1];               //端口
    char telnum[EEPROM_FILED_LENGTH+1];             //电话号码
    int bufsize;            //缓存大小

}EEPROM_CONFIG_DATA;

extern void write_config_data_to_eeprom_api(char* data);
extern char* read_data_eeprom(void);
extern void write_data_eeprom(char* data);
extern void parser_config_data(char *data);
extern void set_default_config_data(void);

extern unsigned char GetComma(unsigned char num,char *str);

extern EEPROM_CONFIG_DATA g_config_data;
#endif
