
#include <rtthread.h>
#include <stm32f10x_iwdg.h>

void watch_dog_config(void)
{
   	/* Enable write access to IWDG_PR and IWDG_RLR registers */
 	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);	
	/* IWDG counter clock: 32KHz(LSI) / 32 = 1KHz */
	IWDG_SetPrescaler(IWDG_Prescaler_128);	
	/* Set counter reload value T=(fn(分频系数)/4)*0.1*RLR(重新装载值)  */
	IWDG_SetReload(0x0fff);	  //500ms
	/* Reload IWDG counter喂狗 */
	IWDG_ReloadCounter();	
	//IWDG_Enable();

}

void enable_wg()
{
	IWDG_Enable();
}
