#include "api/api.h"
#include "api/init.h"

#include <string.h>
#include <stdio.h>

#include <rtthread.h>

#define DTU_SERIAL_PORT USART_PORT_1 //使用串口1作为数据传输口


static void recv_serial_data(USART_PORT_NUM port,unsigned char *buffer , unsigned int size)
{
	unsigned char eeprom_buf[16] = {0x0};
	if (size == 3)
	{
		if (strstr(buffer,"read eeprom") == buffer)
		{
			ApiReadEEPROM(16,eeprom_buf,16);
			ApiWriteSerial(port,eeprom_buf,sizeof(eeprom_buf));
		}
		else if (strstr(buffer,"write eeprom ") == buffer)
		{
			snprintf(eeprom_buf,sizeof(eeprom_buf),"%s",buffer+strlen("write eeprom "));
			if (ApiWriteEEPROM(16,eeprom_buf,sizeof(eeprom_buf)) == EI_OK)
				ApiSerialPrintf(port,"write eeprom ok.\n");
			else
				ApiSerialPrintf(port,"write eeprom failed.\n");
				
		}
	}

}

void  StartupImpl(void)
{
}
void RecvSysEventImpl(EI_SYS_EVENT event , void *body)
{

	switch(event)
	{
		case RECVEVENT_SERIAL: //串口有信息到来
			recv_serial_data(
					((struct RcvSerialDataEvent*)body)->port,
					((struct RcvSerialDataEvent*)body)->data,
					((struct RcvSerialDataEvent*)body)->len
					);
			break;
		default:
			break;
	}
	//
}

